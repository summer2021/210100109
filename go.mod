module dubbo

go 1.14

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.0-rc2
	github.com/SkyAPM/go2sky v1.2.0
	github.com/dubbogo/gost v1.11.16 // indirect
	github.com/google/uuid v1.3.0 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210813162853-db860fec028c // indirect
	google.golang.org/grpc v1.40.0 // indirect
	skywalking.apache.org/repo/goapi v0.0.0-20210804062511-c324007aab53
)
