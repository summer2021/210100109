package main

import (
	"log"

	"dubbo.apache.org/dubbo-go/v3/config"
	"github.com/SkyAPM/go2sky"
	"github.com/SkyAPM/go2sky/reporter"
)

const (
	oap     = "mockoap:19876"
	service = "dubbo-server"
)

type UserProvider struct {
}

func (u *UserProvider) Reference() string {
	return "UserProvider"
}
func (u *UserProvider) Reference() string {
	return "UserProvider"
}

func main() {
	report, err := reporter.NewGRPCReporter(oap)
	if err != nil {
		log.Fatalf("crate grpc reporter error: %v \n", err)
	}

	tracer, err := go2sky.NewTracer(service, go2sky.WithReporter(report))
	if err != nil {
		log.Fatalf("crate tracer error: %v \n", err)
	}

	config.SetProviderService(new(UserProvider))
	config.Load()
}
