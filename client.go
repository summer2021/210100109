package dubbo_client

import (
	"context"
	"sync"
	"time"

	"dubbo.apache.org/dubbo-go/v3/common/extension"
	"dubbo.apache.org/dubbo-go/v3/filter"
	"dubbo.apache.org/dubbo-go/v3/protocol"
	agentv3 "skywalking.apache.org/repo/goapi/collect/language/agent/v3"

	"github.com/SkyAPM/go2sky"
	"github.com/SkyAPM/go2sky/internal/tool"
)

const (
	// actually didn't know how to define this codes exactly
	componentIDGo2SkyClient = 5013
	componentIDGo2SkyServer = 5014

	errInvalidTracer = tool.Error("invalid tracer")
)

// this should be executed before users set their own Tracer
func init() {
	// init default tracer
	defaultTracer, _ = go2sky.NewTracer("default-dubbo-go-tracer")

	// set filter
	// in dubbo go conf:
	//     client side: filter: "go2sky-tracing-client"
	//     server side: filter: "go2sky-tracing-server"
	// maybe move to constant
	extension.SetFilter("go2sky-tracing-client", GetClientTracingFilterSingleton)
	extension.SetFilter("go2sky-tracing-server", GetServerTracingFilterSingleton)
}

// default go2sky tracer
var defaultTracer *go2sky.Tracer

type tracingFilter struct {
	tracer      *go2sky.Tracer
	extraTags   map[string]string
	reportTags  []string
	componentID int
}

// SetTracer set user's tracer.
func (cf *tracingFilter) SetTracer(tracer *go2sky.Tracer) error {
	if tracer == nil {
		return errInvalidTracer
	}

	cf.tracer = tracer

	return nil
}

// SetExtraTags adds extra tag to spans.
func (cf *tracingFilter) SetExtraTags(key string, value string) {
	if cf.extraTags == nil {
		cf.extraTags = make(map[string]string)
	}

	cf.extraTags[key] = value
}

// SetReportTags adds report tags to spans.
func (cf *tracingFilter) SetReportTags(tags ...string) {
	cf.reportTags = append(cf.reportTags, tags...)
}

// Invoke implements dubbo-go filter interface.
func (cf tracingFilter) Invoke(ctx context.Context, invoker protocol.Invoker, invocation protocol.Invocation) protocol.Result {
	operationName := invoker.GetURL().ServiceKey() + "#" + invocation.MethodName()

	var span go2sky.Span

	if cf.componentID == componentIDGo2SkyClient {
		span, _ = cf.tracer.CreateExitSpan(ctx, operationName, invoker.GetURL().ServiceKey(), func(key, value string) error {
			invoker.GetURL().AddParam(key, value)

			return nil
		})

		span.SetComponent(componentIDGo2SkyClient)
	} else {
		// componentIDGo2SkyServer
		span, ctx, _ = cf.tracer.CreateEntrySpan(ctx, operationName, func(key string) (string, error) {
			return invocation.AttachmentsByKey(key, ""), nil
		})

		span.SetComponent(componentIDGo2SkyServer)
	}

	// add extra tags
	for k, v := range cf.extraTags {
		span.Tag(go2sky.Tag(k), v)
	}

	// add report tags
	for _, tag := range cf.reportTags {
		// from attachments
		if v, ok := invocation.Attachments()[tag].(string); ok {
			if ok {
				span.Tag(go2sky.Tag(tag), v)
			}
		}
		// or from url
		if v := invoker.GetURL().GetParam(tag, ""); v != "" {
			span.Tag(go2sky.Tag(tag), v)
		}
	}

	// other tags ...
	span.SetSpanLayer(agentv3.SpanLayer_RPCFramework)

	result := invoker.Invoke(ctx, invocation)

	// finish span
	defer span.End()
	if result.Error() != nil {
		// tag error
		span.Error(time.Now(), result.Error().Error())
	}

	return result
}

// OnResponse implements dubbo-go filter interface.
func (cf tracingFilter) OnResponse(ctx context.Context, result protocol.Result, invoker protocol.Invoker, invocation protocol.Invocation) protocol.Result {
	return result
}

var (
	ServerFilterOnce sync.Once
	ServerFilter     *tracingFilter

	ClientFilterOnce sync.Once
	ClientFilter     *tracingFilter
)

// GetServerTracingFilterSingleton returns global server filter for server side.
func GetServerTracingFilterSingleton() filter.Filter {
	ServerFilterOnce.Do(func() {
		ServerFilter = &tracingFilter{
			tracer: defaultTracer,
		}
	})
	return ServerFilter
}

// GetClientTracingFilterSingleton returns global filter for client side.
func GetClientTracingFilterSingleton() filter.Filter {
	ClientFilterOnce.Do(func() {
		ClientFilter = &tracingFilter{
			tracer: defaultTracer,
		}
	})
	return ClientFilter
}
